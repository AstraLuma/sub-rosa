mod config;
mod feed;
mod miniflux;
mod parse;

use std::io::Write;
use std::process::Command;
use std::{collections::HashSet, path::PathBuf};

use crate::config::Config;
use crate::feed::FeedConverter;

use anyhow::{ensure, Result};
use imap::types::{Fetch, Seq};
use itertools::Itertools;
use log::{debug, error, info, LevelFilter};

use miniflux_api::MinifluxApi;
use native_tls::TlsConnector;
use std::fs;
use std::fs::File;
use structopt::StructOpt;
use tempdir::TempDir;

/// Dumps the file to the given directory, if requested.
fn dump(opt: &Opt, name: String, contents: &[u8]) -> Result<()> {
    if let Some(dump_path) = &opt.dump {
        let path = dump_path.join(name);
        debug!("Dumping to {}", path.display());
        File::create(path)?.write_all(contents)?;
    }
    Ok(())
}

/// Given a bunch of message IDs, return the string to fetch them.
fn sequence_set_string(set: &HashSet<Seq>) -> String {
    let mut seqs: Vec<Seq> = set.iter().copied().collect();
    seqs.sort_by(|a, b| b.cmp(a));
    // TODO: eventually replace this with date-based limiting
    seqs.truncate(100);
    seqs.iter().join(",")
}

/// Call the given closure for each Patreon post. Returns true if at
/// least one post errored while being processed.
fn for_each_post<F>(imap: &config::Imap, mut f: F) -> Result<bool>
where
    F: FnMut(&Fetch) -> Result<()>,
{
    let tls = TlsConnector::new()?;
    let client = imap::connect((imap.server.as_str(), 993), &imap.server, &tls)?;
    let mut session = client
        .login(&imap.username, &imap.password)
        .map_err(|e| e.0)?;
    debug!("Connected to server {}", imap.server);
    session.examine("INBOX")?;

    // Get the 10 most recent posts. bingo@patreon.com is their address for sending stuff from.
    let seqs = session.search("FROM \"bingo@patreon.com\"")?;
    debug!("Fetching messages {:?}", seqs);
    let fetch = session.fetch(sequence_set_string(&seqs), "BODY[]")?;
    let mut had_error = false;
    for msg in fetch.into_iter() {
        // it's allowed for servers to send extra messages, so ignore them
        if seqs.contains(&msg.message) {
            if let Err(err) = f(msg) {
                error!("Error processing message {}: {:?}", msg.message, err);
                had_error = true;
            }
        }
    }
    Ok(had_error)
}

#[derive(Debug, StructOpt)]
struct Opt {
    /// Path to load the config file from.
    #[structopt(short, long, parse(from_os_str))]
    config: Option<PathBuf>,

    /// Dump out the e-mail to the given directory, which should exist.
    #[structopt(long, parse(from_os_str))]
    dump: Option<PathBuf>,

    /// If true, keep the temporary directory used to store the output.
    #[structopt(short, long)]
    keep_temporary: bool,

    /// Don't add new feeds to the Miniflux instance (if configured).
    #[structopt(long)]
    skip_add_feeds: bool,

    /// Enables more logging. This can include URLs with access
    /// tokens, so don't enable this except when running
    /// interactively!
    #[structopt(short, long)]
    verbose: bool,
}

#[tokio::main]
async fn main() -> Result<()> {
    let opt = Opt::from_args();

    let log_level = if opt.verbose {
        LevelFilter::Debug
    } else {
        LevelFilter::Info
    };
    env_logger::Builder::new()
        .filter(Some("sub_rosa"), log_level)
        .format_timestamp(None)
        .init();
    let config = match &opt.config {
        Some(path) => Config::load_from(path)?,
        None => Config::load()?,
    };
    let mut converter = FeedConverter::new();

    for_each_post(&config.imap, |msg| {
        let opt = &opt;
        let message = msg;
        let body = message
            .body()
            .expect("Didn't get headers even though we asked?");
        dump(opt, format!("{}.raw", message.message), body)?;
        let mail = mailparse::parse_mail(body)?;
        let html = parse::find_html(&mail)?;
        dump(opt, format!("{}.html", message.message), html.as_bytes())?;
        let info = parse::parse_mail(&mail, &html)?;
        debug!("Got info: {:?}", info);
        converter.add_post(info);
        Ok(())
    })?;

    let tmp_dir = TempDir::new("sub-rosa")?;
    let tmp_path = tmp_dir.path().to_path_buf();
    info!("Using {} as a temp path", tmp_path.display());
    if opt.keep_temporary {
        std::mem::forget(tmp_dir);
    }

    let feeds = converter.feeds()?;
    for (creator, feed) in feeds.iter() {
        let path = tmp_path.join(creator.path());
        fs::create_dir_all(path.parent().unwrap())?;
        File::create(path)?.write_all(feed.to_string().as_bytes())?;
    }

    info!("Syncing with {:?}", &config.hosting.sync_command);
    let status = Command::new("/bin/sh")
        .env("SUB_ROSA_OUTPUT", tmp_path)
        .args(&["-c", &config.hosting.sync_command])
        .status()?;
    ensure!(status.success(), "Sync command failed");

    let hosted_urls = feeds.keys().map(|creator| {
        config
            .hosting
            .base_url
            .join(&creator.path().to_string_lossy())
            .unwrap()
    });

    if let Some(miniflux) = &config.miniflux {
        if !opt.skip_add_feeds {
            let mut api = MinifluxApi::new(
                &miniflux.url,
                miniflux.username.clone(),
                miniflux.password.clone(),
            );
            miniflux::add_missing_feeds(
                &mut api,
                hosted_urls,
                &miniflux.default_category,
                &config.hosting,
            )
            .await?;
        }
    };
    Ok(())
}
