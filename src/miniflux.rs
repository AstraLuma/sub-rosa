use std::collections::HashSet;

use anyhow::{anyhow, Result};
use log::{debug, error, info};
use miniflux_api::{models::Category, MinifluxApi};
use url::Url;

use crate::config::Hosting;

pub async fn add_missing_feeds<T>(
    api: &mut MinifluxApi,
    feeds: T,
    default_category: &str,
    hosting: &Hosting,
) -> Result<()>
where
    T: Iterator<Item = Url>,
{
    let client = reqwest::Client::new();
    let current_feeds: HashSet<Url> = api
        .get_feeds(&client)
        .await
        .map_err(|e| anyhow!("While getting feeds: {}", e))?
        .into_iter()
        .filter_map(|feed| Url::parse(&feed.feed_url).ok())
        .collect();

    let category = get_or_create_category(api, &client, default_category).await?;

    for feed in feeds {
        if current_feeds.contains(&feed) {
            debug!("Feed {} already present", feed);
            continue;
        }
        if let Err(err) = add_feed(&feed, &category, hosting, api, &client).await {
            error!("Couldn't create feed for {}: {}", &feed, err);
        }
    }
    Ok(())
}

/// Create the feed and set its username/password.
async fn add_feed(
    url: &Url,
    category: &miniflux_api::models::Category,
    hosting: &Hosting,
    api: &mut MinifluxApi,
    client: &reqwest::Client,
) -> Result<()> {
    info!("Adding feed {}", url);
    api.create_feed(
        url,
        category.id,
        Some(&hosting.username),
        Some(&hosting.password),
        client,
    )
    .await
    .map_err(|e| anyhow!("create_feed failed: {}", e))?;
    Ok(())
}

/// Either get the category with the given name or create it.
async fn get_or_create_category(
    api: &mut MinifluxApi,
    client: &reqwest::Client,
    name: &str,
) -> Result<Category> {
    let categories = api
        .get_categories(&client)
        .await
        .map_err(|e| anyhow!("Failed getting categories: {}", e))?;
    // XXX: miniflux_api's category creation API expects the wrong status code
    match categories.into_iter().find(|cat| &cat.title == name) {
        Some(cat) => Ok(cat),
        None => api
            .create_category(name, &client)
            .await
            .map_err(|e| anyhow!("create_category failed: {}", e)),
    }
}
