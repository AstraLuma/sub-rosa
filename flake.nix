{
  description = "Converter from Patreon e-mails to RSS feeds";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
    # Can unpin when
    # https://github.com/nix-community/naersk/issues/193 is fixed.
    naersk.url =
      "github:nix-community/naersk?rev=df71f5e4babda41cd919a8684b72218e2e809fa9";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        naersk-lib = naersk.lib."${system}";
      in rec {
        # `nix build`
        packages.sub-rosa = naersk-lib.buildPackage {
          pname = "sub-rosa";
          root = ./.;
          buildInputs = with pkgs; [ pkg-config openssl ];
        };
        defaultPackage = packages.sub-rosa;

        # `nix run`
        apps.sub-rosa = utils.lib.mkApp { drv = packages.sub-rosa; };
        defaultApp = apps.sub-rosa;

        # `nix develop`
        devShell = pkgs.mkShell {
          inputsFrom = [ defaultPackage ];
          nativeBuildInputs = with pkgs; [
            clippy
            rustfmt
            rust-analyzer
            cargo
            rustc
          ];
        };

      }) // {
        overlay = final: prev: {
          sub-rosa = self.defaultPackage.${prev.system};
        };
        nixosModule = import ./module.nix;
      };
}
