{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.sub-rosa;
  tomlFormat = pkgs.formats.toml { };

in {
  options.services.sub-rosa = {
    enable = mkEnableOption "Patreon-RSS email bridge";

    package = mkOption {
      type = types.package;
      default = pkgs.sub-rosa;
      defaultText = literalExample "pkgs.sub-rosa";
      description = "sub-rosa derivation to use.";
    };

    onCalendar = mkOption {
      type = types.str;
      default = "*-*-* *:0/10:00";
      description = "How often to run, in systemd.time syntax.";
    };

    config = mkOption {
      type = tomlFormat.type;
      description = "Configuration in the same format as the config.toml.";
    };

    user = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "What user to run the job as.";
    };

    path = mkOption {
      type = types.listOf types.package;
      default = [ pkgs.openssh ];
      description = "Additional packages to put on the service's path.";
    };

    envFile = mkOption {
      type = types.nullOr types.path;
      default = null;
      description = "A systemd env file. Useful for storing your passwords.";
    };
  };

  config = mkIf cfg.enable {
    systemd.services.sub-rosa = {
      startAt = cfg.onCalendar;
      path = cfg.path;
      script = "${cfg.package}/bin/sub-rosa -c ${
          tomlFormat.generate "sub-rosa-config" cfg.config
        }";
      description = "Converter from Patreon e-mails to RSS feeds";

      serviceConfig = {
        Type = "oneshot";
        User = mkIf (cfg.user != null) cfg.user;
        EnvironmentFile = mkIf (cfg.envFile != null) [ cfg.envFile ];
      };
    };
  };
}
